#+OPTIONS: toc:nil
#+OPTIONS: num:nil
#+OPTIONS: author:nil date:nil
#+OPTIONS: reveal_single_file:t reveal_title_slide:nil

#+TITLE: HashiStack Overview - Terraform and Vault
#+AUTHOR: Nathan Valentine
#+EMAIL: nathan@hashicorp.com

#+REVEAL_THEME: blood
#+REVEAL_TRANS: linear
#+REVEAL_EXTRA_CSS: ./style.css
#+REVEAL_INIT_SCRIPT: height: 800

* 
 [[../../assets/images/HashiCorp_Icon_White-small.png]]
 - [[https://bit.ly/2JLHAP6]]

** Your Account Manager and Solutions Engineer
|          Kimberly Chu          |            Nathan Valentine             |
|--------------------------------+-----------------------------------------|
|              <c>               |                   <c>                   |
| [[../../assets/people/KimChu.jpg]] | [[../../assets/people/NathanValentine.png]] |
|   Enterprise Account Manager   |        Senior Solutions Engineer        |
|    kimberly@hashicorp.com      |          nathan@hashicorp.com           |

** Founders
|                           Mitchell Hashimoto                           |                           Armon Dadgar                           |
|------------------------------------------------------------------------+------------------------------------------------------------------|
|                                  <c>                                   |                               <c>                                |
| [[../../assets/branding/HashiCorp/Photography/JPEG/MitchellHashimoto.jpg]] | [[../../assets/branding/HashiCorp/Photography/JPEG/ArmonDadgar.jpg]] |
|                                                                        |                                                                  |

** Mission
   "We enable organizations to Provision, Secure, Connect, and Run any infrastructure for any application."

** Details
  - Founded in 2012
  - First Open Source project was [[https://vagrantup.com][Vagrant]]
  - Many more OSS projects soon thereafter
    - Packer
    - Consul
    - Vault
    - Nomad
  - Commercial Enterprise offerings in 2015

** Taos of HashiCorp
   - [[https://tinyurl.com/yd9kro99]]
     - "Workflows, not Technologies"
     - "Simple, Modular, Composable"
     - "Resilient Systems"
     - "Pragmatism"
     - ..
** Our Suite
   [[../../assets/branding/HashiCorp/Stack Graphic/PNG/HashiCorpSuite_OpenSource_StackGraphic.png]]

** Things we enable

   | Product   | Use-case                             |
   |-----------+--------------------------------------|
   | [[https://terraform.io][Terraform]] | multi-cloud provisioning             |
   | [[https://consul.io][Consul]]    | service discovery & health-checking  |
   | [[https://vaultproject.io][Vault]]     | dynamic secrets & identity brokering |
   | [[https://nomadproject.io][Nomad]]     | container/VM/cluster/etc scheduling  |
   | [[https://docs.hashicorp.com/sentinel/][Sentinel]]  | governance of *All The Things*       |

* Today's Topics
   - IaaC w/ Terraform Enterprise
   - Identity and Secrets w/ Vault Enterprise

* Terraform Enterprise

** Infra-as-Code
      - multi-cloud
      - re-use & sharing
      - module versioning
      - state files and TFE

** Policy-as-Code
      - Sentinel
      - Terraform & Sentinel

**  Terraform Enterprise
   - ([[https://github.com/nrvale0/terraform-aws-webapp][demo here]])
     - PR workflow
     - Private Module Registry
     - Workspace Designer

** Terraform w/ CI/CD
   - [[https://www.terraform.io/docs/enterprise/run/cli.html][workflow docs]]

   - [[https://github.com/hashicorp/tfe-cli/][tfe-cli]] (supported)
   - [[https://github.com/skierkowski/terraform-enterprise-cli][terraform-enterprise-cli]] (new!)
   - [[https://www.terraform.io/docs/enterprise/api/][REST]]

** Terraform w/ CI/CD
   - ([[https://github.com/nrvale0/vault-concourse][demo here]])
    - [[https://concourse-ci.org][Concourse CI]]
      - API usage

* Vault Enterprise

** Vault Intro
  (presenter's notes: see Vault Technical Deck)

** Interacting with Vault k/v
   - [[https://www.vaultproject.io/api/libraries.html][client libraries]]
   - [[https://www.vaultproject.io/docs/commands/][Vault CLI]]
   - REST/httpie/curl

** Tokens, Leases, and TTLs

** Database credentials as "dynamic" Secrets
   ([[https://gitlab.com/nrvale0/hashistack-workshops/tree/master/vault/2018.05.21/use-cases/database][demo]] here)

** Integrations
*** AWS
    - [[https://www.vaultproject.io/docs/auth/index.html][Auth Method]]
    - [[https://www.vaultproject.io/docs/secrets/aws/index.html][Secrets Engine]]
    - other clouds have similar coverage

*** Kubernetes
    - Secrets [[https://www.vaultproject.io/docs/auth/kubernetes.html][delivered]] to k8s workloads/Pods
    - lots more coming here!
    - Need dynamic Secrets? Consider [[https://nomadproject.io][Nomad]].

** Other Fun Things

** SSH Secrets Engine
   - dynamic SSH auth via certs
   - created on-demand
   - TTL (as low as you'd like)
   - no key management!

** PKI Secrets Engine
   - programmable (via REST)
   - Root or Intermediate CAs
   - dynamic throughout: CA and certs!
   - Operations loves this!

** Encryption as a Service (EaaS)
   - aka "Transit Secret Engine"
   - Most people don't know about this.
   - AppDev loves this!

** SAML and MFA

* Questions?
  - Nathan : nathan@hashicorp.com
  - Kim: kimberly@hashicorp.com
