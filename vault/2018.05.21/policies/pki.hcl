path "pki_root" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "pki_root/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "pki_intermediate" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

path "pki_intermediate/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
