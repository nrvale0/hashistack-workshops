#doitlive commentecho: true

set -x
export VAULT_ADDR=http://localhost:8200
vault login token=vaultroot
vault policy write admin policies/admin.hcl
vault policy write postgres-admin policies/postgres-admin.hcl
vault policy write pki policies/pki.hcl
vault auth enable userpass
vault write auth/userpass/users/vaultadmin password=vaultadmin policies=admin,postgres-admin,pki
vault login -method=userpass username=vaultadmin password=vaultadmin
