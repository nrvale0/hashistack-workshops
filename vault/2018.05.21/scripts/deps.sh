#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -uex

# the internet says this is better than 'set -e'
function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR


# check the local system for required tooling
function deps () {
    local tools='docker-compose jq'
    
    for tool in $tools; do
	command -v $tool > /dev/null 2>&1
	if [ $? ]; then
	    echo "Found required tooling: $tool..."
	else
	    echo "Was not able to find required tooling $tool in PATH. Please install or adjust PATH."
	    exit -1
	fi
    done
}

# all the fun starts here
function main () {
    deps
}


main
