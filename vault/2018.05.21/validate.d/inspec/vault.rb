puts "\nValidating Vault....\n"

VAULT_ADDR='http://vault:8200'

puts "VAULT_ADDR: #{VAULT_ADDR}..."

describe http("#{VAULT_ADDR}/v1/sys/leader") do
  its('status') { should cmp 200 }
end

describe http("#{VAULT_ADDR}/v1/sys/init") do
  its('status') { should cmp 200 }
end

describe http("#{VAULT_ADDR}/v1/sys/seal-status") do
  its('status') { should cmp 200 }
end

describe http("#{VAULT_ADDR}/v1/auth/userpass/users/vaultadmin",
              headers: { 'X-Vault-Token' => 'vaultroot' }) do
  its('status') { should cmp 200 }
end
