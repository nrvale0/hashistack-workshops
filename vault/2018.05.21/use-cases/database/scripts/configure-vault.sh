#doitlive shell: /bin/bash
#doitlive prompt: default

#
# First, let's login as the 'vaultadmin' user which has full access to Vault...
#
vault login -method=userpass username=vaultadmin password=vaultadmin

#
# Let's mount a clean instance of the Vault Database Secret Engine...
#
vault secrets disable database
vault secrets enable database

#
# Next we configure the mounted Database Secret Engine with a PG connection string, etc...
#
vault write database/config/testdb \
      plugin_name=postgresql-database-plugin \
      allowed_roles="writeall,readonly", \
      connection_url="postgresql://{{username}}:{{password}}@postgres:5432/testdb?sslmode=disable" \
      username="postgres" \
      password="postgres"

#
# Now we create the Vault roles to associate with write/read privileges in the PG database...
#

#
# Create the Vault policy for write-all credentials...
#
vault write database/roles/writeall \
      db_name=testdb \
      creation_statements="create role \"{{name}}\" with login password '{{password}}' valid until '{{expiration}}'; grant all on all tables in schema public to \"{{name}}\";" \
      default_ttl="10m" \
      max_ttl="1h"

#
# Create the Vault policy for read-only credentials...
#
vault write database/roles/readonly \
      db_name=testdb \
      creation_statements="create role \"{{name}}\" with login password '{{password}}' valid until '{{expiration}}'; grant select on all tables in schema public to \"{{name}}\";" \
      default_ttl="10m" \
      max_ttl="1h"

#
# Lastly, let's get the database credentials for both the write and read roles...
#

#
# Get temporary credentials for write-all access to the database...
#
vault read database/creds/writeall

#
# Get temporary credentials for read-only access to the database...
#
vault read database/creds/readonly
