#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

# the internet says this is better than 'set -e'
function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR

# create the test database
function create_database () {
    (set -x && \
	 export PGHOST=localhost && \
	 export PGUSER=postgres && \
	 export PGPASSWORD=postgres && \
	 sleep 3 && \
	 dropdb testdb && \
	 createdb testdb && \
	 psql testdb -1 -f ./testdb.dump)
}

# all the fun starts here
function main () {
    create_database
}

main
