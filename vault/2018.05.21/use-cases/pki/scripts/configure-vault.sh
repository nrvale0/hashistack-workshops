#doitlive shell: /bin/bash
#doitlive prompt: default

#
# First, let's login as the 'vaultadmin' user which has full access to Vault...
#
vault login -method=userpass username=vaultadmin password=vaultadmin

#
# Let's mount a clean instance of the Vault PKI Secret Engine...
#
vault secrets disable pki
vault secrets enable pki
vault secrets tune -max-lease-ttl=8760h pki

#
# Create an Intermediate CA from the EasyRSA-based Root CA...
#
vault write pki/root/
