#!/bin/bash

[ ! -n "$DEBUG" ] || set -x

set -ue

# the internet says this is better than 'set -e'
function onerr {
    echo 'Cleaning up after error...'
    exit -1
}
trap onerr ERR


function main () {
    docker rm -v $(docker ps -aq --filter "name=validate-postgres") > /dev/null 2>&1 || true
    docker-compose run -T postgres-validate
}


main
