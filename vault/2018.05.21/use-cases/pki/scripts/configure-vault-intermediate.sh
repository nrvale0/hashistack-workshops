#doitlive shell: /bin/bash
#doitlive prompt: default
#doitlive speed: 5
#doitlive commentecho: true

set -eux

#
# Set the vault server address..
#
export VAULT_ADDR=http://localhost:8200

#
# Login as the vaultadmin user which has full access to Vault...
#
vault login -method=userpass username=vaultadmin password=vaultadmin

#
# Let's mount a clean instance of the Vault PKI Secret Engine...
#
vault secrets disable pki_intermediate
vault secrets enable -path pki_intermediate pki
vault secrets tune -max-lease-ttl=8760h pki_intermediate

#
# Create an Intermediate CA...
#
vault write -format=json pki_intermediate/intermediate/generate/internal \
      common_name="subdomain.somefakeorg.example Intermediate CA" \
      ttl=8760h | jq -r .data.csr > /tmp/intermediate.csr

#
# Submit the Intermediate CSR to the Root CA for signing...
#
vault write -format=json pki_root/root/sign-intermediate \
      ttl=8760h \
      csr=@/tmp/intermediate.csr | jq -r .data.certificate > /tmp/intermediate.crt

#
# Take the Root CA-signed certificate and set it as the Intermediate CA which
# will be used for signing of certificates in the Intermediate CA...
#
vault write pki_intermediate/intermediate/set-signed \
      certificate=@/tmp/intermediate.crt

#
# Create role that can be used to authorize creation of certificates with a CN
# in the subdomain(s)...
#
vault write pki_intermediate/roles/subdomain.somefakeorg.example \
      allowed_domains=somefakeorg.example \
      lease_max="336h" \
      allow_subdomains=true

#
# Create a certificate with CN=foo.subdomain.fakeorg.example...
#
vault write --format=json pki_intermediate/issue/subdomain.somefakeorg.example \
      common_name="foo.somefakeorg.example" \
      ttl="168h" | jq -r .data.private_key,.data.certificate,.data.issuing_ca > /tmp/foo.somefakeorg.example.pem

#
# Output the new certificates metadata for inspection...
#
openssl x509 -in /tmp/foo.somefakeorg.example.pem -text
