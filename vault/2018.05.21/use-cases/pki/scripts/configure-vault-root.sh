#doitlive shell: /bin/bash
#doitlive prompt: default
#doitlive speed: 5
#doitlive commentecho: true

set -eux

#
# Set the vault server address..
#
export VAULT_ADDR=http://localhost:8200

#
# Login as the vaultadmin user which has full access to Vault...
#
vault login -method=userpass username=vaultadmin password=vaultadmin

#
# Mount a clean instance of the Vault PKI Secret Engine...
#
vault secrets disable pki_root
vault secrets enable -path=pki_root pki
vault secrets tune -max-lease-ttl=8760h pki_root

#
# Create a Root CA...
#
vault write -format=yaml pki_root/root/generate/internal \
      common_name='somefakeorg.example Root CA' \
      ttl=8760h
